# Installing MAVS Binaries
MAVS binaries are available for Windows 10. The binary comes in the form of a .dll file and a python library for interfacing to the dll.

1. Clone the mavs binaries from https://gitlab.com/cgoodin/mavs-binaries. ([Request the binaries here](http://www.cavs.msstate.edu/capabilities/mavs_request.php)).
	* $git clone https://gitlab.com/cgoodin/mavs-binaries.git
    * It is important that there are no spaces in the full file path, so the C drive is a good place to install MAVS.
2. Edit the file "C:\mavs_windows10\mavs_python\mavs_python_paths.py" to point to the location of the MAVS dll and data folder on your machine.
3. Add the folder "C:\mavs_windows10\lib" to your system path (see  instructions in follwing section).
4. Edit line  of the file "C:\mavs_windows10\simulation_example.py" to point to the location of the "C:\mavs_windows10\mavs_python" folder on your machine.
5. Run the simulation with "python simulation_example.py"
6. The first time you run MAVS, a pop-up dialog will ask you to provide the mavs data path. This should be the full path to "C:\mavs_windows10\data".

## Adding MAVS to Your System Path

![Slide 1](./adding_mavs_dll_to_path_images/Slide1.png)
![Slide 2](./adding_mavs_dll_to_path_images/Slide2.png)
![Slide 3](./adding_mavs_dll_to_path_images/Slide3.png)
![Slide 4](./adding_mavs_dll_to_path_images/Slide4.png)
![Slide 5](./adding_mavs_dll_to_path_images/Slide5.png)
![Slide 6](./adding_mavs_dll_to_path_images/Slide6.png)
![Slide 7](./adding_mavs_dll_to_path_images/Slide7.png)
![Slide 8](./adding_mavs_dll_to_path_images/Slide8.png)
![Slide 9](./adding_mavs_dll_to_path_images/Slide9.png)
